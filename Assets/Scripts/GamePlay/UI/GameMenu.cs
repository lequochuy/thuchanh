using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    public static GameMenu Instance;
    [SerializeField] private GameObject hudMenu;
    [SerializeField] private PlayerHealthBar playerHealthBar;
    [SerializeField] private PauseMenu pauseMenu;
    void Awake()
    {
        Instance = this;
    }
    
    public void UpdateHeath(float amount)
    {
        playerHealthBar.UpdateHeath(amount);
    }

    public void PauseGame()
    {
        hudMenu.SetActive(false);
        pauseMenu.Open();
        GameManager.Instance.Pause();
    }

    public void ResumeGame()
    {
        hudMenu.SetActive(true);
        pauseMenu.Close();
        GameManager.Instance.Resume();
    }

    public void ResetGame()
    {
        ResumeGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BackToMain()
    {
        ResumeGame();
        SceneManager.LoadScene("Menu");
    }
}
