using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    [SerializeField] private Image health;

    public void UpdateHeath(float amount)
    {
        health.fillAmount = amount;
    }
}
