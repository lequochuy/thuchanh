using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private float speed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Follow(GameManager.Instance.playerManager.transform.position);
    }

    void Follow(Vector3 position)
    {
        position.z = transform.position.z;
        // C�i n�y l� s? di chuy?n t? th?ng 1 sang th?ng th? 2
        _camera.transform.position = Vector3.Slerp(transform.position, position, Time.deltaTime * speed);
    }
}
