using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer crossHair;
    void Start()
    {
        
    }

    void Update()
    {
        crossHair.enabled = false;
        if (Input.GetMouseButton(0))
        {
            crossHair.enabled = true;
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = pos;
        }
    }
}
