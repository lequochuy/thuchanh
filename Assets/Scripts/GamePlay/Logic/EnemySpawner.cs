using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject zombiePrefab;
    [SerializeField] private int amount = 10;
    [SerializeField] private float rate = 5f;
    private float timer;

    private void Start()
    {
        GameManager.Instance.enemyCount += amount;
        GameManager.Instance.UpdateEnemyCount();
    }

    public void Update()
    {
        if (timer < 0)
        {
            SpawnZombie();
            timer = rate;
        }

        timer -= Time.deltaTime;
    }

    public void SpawnZombie()
    {
        if (amount < 0)
        {
            return;
        }
        var zombie = Instantiate(zombiePrefab, transform.position, Quaternion.identity);
        amount--;
    }
}
