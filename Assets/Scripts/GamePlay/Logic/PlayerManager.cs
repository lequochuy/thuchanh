using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : BaseCharactor
{
    [SerializeField] Transform lookTarget;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] BulletSpawner spawner;

    private float _bulletTime;
    void Start()
    {
        
    }

    void Update()
    {
        CheckRotation();
        _bulletTime -= Time.deltaTime;
    }

    void FixedUpdate()
    {
        CheckMovement();
    }

    void CheckMovement()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        Move(x, y);
    }

    // C�i n�y de chuyen g�c quay v�o hong t�m
    void CheckRotation()
    {
        if (!Input.GetMouseButton(0)) return;
        float angle = Vector2.SignedAngle(lookTarget.position - transform.position, Vector2.up);
        // transform.eulerAngles = new Vector3(0, 0, -angle);
        rigid.MoveRotation(-angle);
        Shoot();
    }

    void Shoot()
    {
        if (_bulletTime > 0) return;
        Bullet bullet = spawner.GetBullet();
        bullet.transform.position = bulletSpawn.transform.position;
        bullet.transform.rotation = transform.rotation;
        bullet.gameObject.SetActive(true);
        _bulletTime = 1 / bullet.fireRate;
    }
    
    public override void UpdateHealthBar(float amount)
    {
        GameMenu.Instance.UpdateHeath(amount);
    }
}
