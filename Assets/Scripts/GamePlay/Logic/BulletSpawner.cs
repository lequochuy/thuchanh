using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject[] bulletPool;
    [SerializeField] public int pollSize = 10;

    // Start is called before the first frame update
    void Start()
    {
        bulletPool = new GameObject[pollSize];
        for (int i = 0; i < pollSize; i++)
        {
            bulletPool[i] = Instantiate(bulletPrefab);
            bulletPool[i].SetActive(false);
        }
    }

    // Update is called once per frame
    public Bullet GetBullet()
    {
        for (int i = 0; i < pollSize; i++)
        {
            GameObject bulletObject = bulletPool[i];
            if (!bulletObject.activeSelf)
            {
                Bullet bullet = bulletObject.GetComponent<Bullet>();
                return bullet;
            }
        }
        return null;
    }
}
