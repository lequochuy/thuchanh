using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float fireRate = 0.5f;
    public float damage = 10f;
    public float speed = 10f;

    private void Update()
    {
        transform.Translate(Vector2.up * Time.deltaTime * speed);
    }

    void DestroyBullet()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall"))
        {
            DestroyBullet();
        }
        
        if (collision.CompareTag("Enemy"))
        {
            DestroyBullet();
            var baseCharactor = collision.GetComponent<BaseCharactor>();
            baseCharactor.TakeDamage(damage);
        }
    }
}
