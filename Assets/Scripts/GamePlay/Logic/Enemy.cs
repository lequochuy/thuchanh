using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : BaseCharactor
{
    [SerializeField] private float damage = 5;
    [SerializeField] private float coolDownAttack = 2;
    [SerializeField] private HealthBar healthBar;
    private float coolDownAttackTimer = 0;
    
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.playerManager != null)
        {
            Follow(GameManager.Instance.playerManager.transform);
        }
        coolDownAttackTimer -=Time.deltaTime;
    }
    
    void Follow(Transform target)
    {
        rigid.velocity = (target.position - transform.position).normalized * Time.deltaTime * 10 * speed;

        float angle = Vector2.SignedAngle(target.position - transform.position, Vector2.up);
        rigid.MoveRotation(-angle);
    }
    
    public override void UpdateHealthBar(float amount)
    {
        healthBar.SetHealth(amount);
    }
    
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && coolDownAttackTimer < 0)
        {
            PlayerManager playerManager = collision.gameObject.GetComponent<PlayerManager>();
            playerManager.TakeDamage(damage);
            coolDownAttackTimer = coolDownAttack;
        }
    }
}
