using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCharactor : MonoBehaviour
{
    [SerializeField] protected float maxHp = 100;
    [SerializeField] protected Rigidbody2D rigid;
    [SerializeField] protected float speed = 10;
    [SerializeField] protected float hp = 100;

    protected void Move(float x, float y)
    {
        var direction = new Vector2(x, y);
        // normalized luon co vector la 1
        rigid.velocity = direction.normalized * speed;
    }
    
    public void TakeDamage(float damage)
    {
        hp -= damage;
        UpdateHealthBar(hp / maxHp);
        if(hp < 0)
        {
            Dead();
        }
    }

    void Dead()
    {
        Destroy(gameObject);
    }

    public abstract void UpdateHealthBar(float amount);
}
