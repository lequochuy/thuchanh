using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Transform health;
    private Vector3 _distance;
    void Start()
    {
        _distance = transform.position - transform.parent.position;
    }

    public void SetHealth(float amount)
    {
        amount = Mathf.Clamp(amount, 0, 1);
        var scale = health.transform.localScale;
        scale.x = amount;
        health.transform.localScale = scale;
    }
    
    public void Update()
    {
        transform.position = transform.parent.position + _distance;
        transform.rotation = Quaternion.identity;

    }
}
