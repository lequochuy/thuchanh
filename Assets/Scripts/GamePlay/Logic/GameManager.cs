using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public PlayerManager playerManager;
    public int enemyCount = 10;
    
    void Awake()
    {
        Instance = this;
    }

    public void UpdateEnemyCount()
    {
    }
    
    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Time.timeScale = 1;
    }
}
